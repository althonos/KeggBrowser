package fr.upsud.bibs.rdml.kegg;

import java.io.File;
import org.jdom2.Document;
import org.jdom2.input.SAXBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ReactionTest {

  private Reaction reaction;
  private File kgmlFile;

  @Rule
  public final ExpectedException exception = ExpectedException.none();

  @Before
  public void setUp() {
    kgmlFile = new File("src/test/resources/kgml/hsa.00232.kgml");
  }

  @Test
  public void testParseReactionFromKGML() {

    Document document;
    SAXBuilder sxb = new SAXBuilder();

    try {
      document = sxb.build(kgmlFile);
      reaction = new Reaction(document.getRootElement().getChild("reaction"));
    } catch (Exception e) {
      e.printStackTrace();
    };

    Assert.assertEquals(Integer.valueOf(35), reaction.getId());
    Assert.assertEquals(reaction.getName(), "rn:R07943");
    Assert.assertEquals(reaction.isReversible(), false);

    Assert.assertEquals(reaction.getSubstrates().size(), 1);
    Assert.assertEquals(reaction.getProducts().size(), 1);
  }

}
