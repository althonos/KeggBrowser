package fr.upsud.bibs.rdml.kegg;

import java.io.File;
import java.util.Arrays;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class GenericPathwayTest {

  private GenericPathway pathway;
  private SinglePathway pathway1, pathway2;
  private File kgmlFile1, kgmlFile2;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
  }

  @Before
  public void setUp() {
    kgmlFile1 = new File("src/test/resources/kgml/hsa.00232.kgml");
    kgmlFile2 = new File("src/test/resources/kgml/ara.00232.kgml");
    try {
      pathway1 = new SinglePathway(kgmlFile1);
      pathway2 = new SinglePathway(kgmlFile2);
    } catch(Exception e) {
      Assert.fail("Could not open KGML document: "+e);
    }
  }

  @Test
  public void testAttributes() {
    pathway = new GenericPathway(pathway1);

    Organism hsa = Organism.fromId("hsa");
    Organism ara = Organism.fromId("ara");

    Assert.assertEquals(Arrays.asList(hsa), pathway.getOrganisms());

    pathway.extend(pathway2);

    Assert.assertEquals(Arrays.asList(hsa, ara), pathway.getOrganisms());
  }


  @Test
  public void testDisplay() throws InterruptedException {
    pathway = new GenericPathway(pathway1);
    pathway.extend(pathway2);
    pathway.getGraph().display(false);
  }


}
