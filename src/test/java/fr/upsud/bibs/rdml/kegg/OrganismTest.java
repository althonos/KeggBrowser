package fr.upsud.bibs.rdml.kegg;

import org.junit.Assert;
import org.junit.Test;

public class OrganismTest {

  @Test
  public void testOrganismFromFile() {
    String id = "hsa";
    Organism org = Organism.fromId(id);

    Assert.assertEquals("Homo sapiens (human)", org.getName());
    Assert.assertEquals("hsa", org.getId());
    Assert.assertEquals(Integer.valueOf(1001), org.getTNumber());
    Assert.assertEquals("Eukaryotes;Animals;Vertebrates;Mammals",
                        org.getClassification());
  }


}
