package fr.upsud.bibs.rdml.kegg;

import org.junit.Assert;
import org.junit.Before;

public class SinglePathwayFromFileTest extends SinglePathwayTest {

  @Before
  public void setUp() {
    try {
      pathway = new SinglePathway(kgmlFile);
    } catch(Exception e) {
      Assert.fail("Could not open KGML document: "+e);
    }
  }
}
