package fr.upsud.bibs.rdml.kegg;

import java.io.File;
import java.util.Arrays;
import org.jdom2.Document;
import org.jdom2.input.SAXBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CompoundTest {

  private Compound compound;
  private File kgmlFile;

  @Rule
  public final ExpectedException exception = ExpectedException.none();

  @Before
  public void setUp() {
    kgmlFile = new File("src/test/resources/kgml/hsa.00232.kgml");
    compound = new Compound(10, "cpd:C00001");
  }

  @Test
  public void testParseCompoundFromKGML() {
    Document document;
    SAXBuilder sxb = new SAXBuilder();
    try {
      document = sxb.build(kgmlFile);
      compound = new Compound(
        document.getRootElement().getChild("reaction").getChild("substrate")
      );
    } catch (Exception e) {
      Assert.fail("Could not open KGML document.");
    };
    Assert.assertEquals(26, compound.getId(), 67);
    Assert.assertEquals("cpd:C13747", compound.getName());
  }

  @Test
  public void testAltNamesUnknownCompound() {
    compound = new Compound(10, "cpd:xxxxx");
    Assert.assertEquals(0, compound.getAltNames().size());
  }

  @Test
  public void testAltNamesWater() {
    compound = new Compound(1, "cpd:C00001");
    Assert.assertEquals(Arrays.asList("H2O", "Water"), compound.getAltNames());
  }
}
