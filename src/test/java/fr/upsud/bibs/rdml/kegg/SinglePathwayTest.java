package fr.upsud.bibs.rdml.kegg;

import java.io.File;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

abstract public class SinglePathwayTest {

  protected SinglePathway pathway;
  protected File kgmlFile = new File("src/test/resources/kgml/hsa.00232.kgml");

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
  }

  @Test
  public void testGraph() throws InterruptedException {
    pathway.getGraph().display(false);
  }

  @Test
  public void testContainersSize() {
    Assert.assertEquals(38, pathway.getEntries().size());
    Assert.assertEquals(10, pathway.getRelations().size());
    Assert.assertEquals(9, pathway.getReactions().size());
  }

  @Test
  public void testContainersNonNullElements() {
    for (Entry entry: pathway.getEntries())
      Assert.assertNotNull(entry);
    for (Relation relation: pathway.getRelations())
      Assert.assertNotNull(relation);
    for (Reaction reaction: pathway.getReactions())
      Assert.assertNotNull(reaction);
  }

  @Test
  public void testName() {
    Assert.assertEquals(pathway.getName(), "path:hsa00232");
  }

  @Test
  public void testNumber() {
    Assert.assertEquals(pathway.getNumber(), Integer.valueOf(232));
  }

  @Test
  public void testLink() {
    Assert.assertEquals(
      "http://www.genome.jp/kegg-bin/show_pathway?org_name=hsa&mapno=00232",
      pathway.getLink().get()
    );
  }

  @Test
  public void testImage() {
    Assert.assertEquals(
      "http://www.genome.jp/kegg/pathway/map/hsa00232.png",
      pathway.getImage().get()
    );
  }

}
