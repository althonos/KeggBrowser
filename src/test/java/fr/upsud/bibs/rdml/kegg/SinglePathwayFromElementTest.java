package fr.upsud.bibs.rdml.kegg;

import org.jdom2.Document;
import org.jdom2.input.SAXBuilder;
import org.junit.Assert;
import org.junit.Before;

public class SinglePathwayFromElementTest extends SinglePathwayTest {

  @Before
  public void setUp() {
    Document document;
    SAXBuilder sxb = new SAXBuilder();
    try {
      document = sxb.build(kgmlFile);
      pathway = new SinglePathway(document.getRootElement());
    } catch(Exception e) {
      Assert.fail("Could not open KGML document: "+e);
    }
  }

}
