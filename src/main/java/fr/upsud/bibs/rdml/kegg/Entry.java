package fr.upsud.bibs.rdml.kegg;

import java.util.Objects;
import java.util.Optional;
import org.jdom2.Element;

/** An entry as defined by KEGG.
 *
 *  @see <a href="http://www.kegg.jp/kegg/xml/docs/">KGML
 *  format specification</a>
 */
public class Entry{

  /** Possible entry types.
   */
  public enum Type {
    Ortholog, Enzyme, Reaction, Gene,
    Group, Compound, Map, Brite, Other,
    Unknown,
  }

  /** The type of the entry.
   */
  protected Type type;

  /** The KEGG id of the entry.
   */
  protected Integer id;

  /** The name of the entry.
   */
  protected String name;

  /** The link to the entry.
   */
  protected String link;

  /** The reaction this entry is
   *  linked to.
   */
  protected String reaction;

  /** The x-coordinate of the entry.
   */
  protected Integer x;

  /** The y-coordinate of the entry.
   */
  protected Integer y;

  /** The width of the entry.
   */
  protected Integer width;

  /** The height of the entry.
   */
  protected Integer height;

  /** Create a new entry from scratch.
   *
   *  @param id the KEGG id of the entry.
   *  @param name the name of the id.
   *  @param link the link to the entry.
   *  @param type the type of the entry.
   *  @param reaction the reaction the entry is linked to.
   *  @param x the x-coordinate of the entry.
   *  @param y the y-coordinate of the entry.
   *  @param width the width of the entry.
   *  @param height the height of the entry.
   */
  public Entry(Integer id, String name, String link,
               Type type, String reaction, Integer x,
               Integer y, Integer width, Integer height) {
    this.id = id;
    this.name = name;
    this.link = link;
    this.type = type;
    this.reaction = reaction;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  /** Create a new entry from scratch.
   *
   *  @param id the KEGG id of the entry.
   *  @param name the name of the id.
   *  @param link the link to the entry.
   *  @param type the type of the entry.
   *  @param x the x-coordinate of the entry.
   *  @param y the y-coordinate of the entry.
   *  @param width the width of the entry.
   *  @param height the height of the entry.
   */
  public Entry(Integer id, String name, String link,
               Type type, Integer x, Integer y,
               Integer width, Integer height) {
    this(id, name, link, type, null, x, y, width, height);
  }

  /** Create a new entry from a KGML "entry"
   *  element.
   *
   *  @param element a KGML "entry" element.
   */
  public Entry(Element element) {
    Objects.requireNonNull(element);

    id = Integer.parseInt(element.getAttributeValue("id"));
    name = element.getAttributeValue("name");
    link = element.getAttributeValue("link");

    //Gets the right type
    switch (element.getAttributeValue("type")) {
      case "ortholog": type = Type.Ortholog; break;
      case "enzyme":   type = Type.Enzyme;   break;
      case "reaction": type = Type.Reaction; break;
      case "gene":     type = Type.Gene;     break;
      case "group":    type = Type.Group;    break;
      case "compound": type = Type.Compound; break;
      case "map":      type = Type.Map;      break;
      case "brite":    type = Type.Map;      break;
      case "other":    type = Type.Other;    break;
      default:
        type = type.Unknown;
        System.err.println("Unknown type: "+element.getAttributeValue("type"));
    }

    // Get the optional reaction attribute
    reaction = element.getAttributeValue("reaction");

    // Get the coordinates
    Element graphics = element.getChild("graphics");
    x      = Integer.valueOf(graphics.getAttributeValue("x"));
    y      = Integer.valueOf(graphics.getAttributeValue("y"));
    width  = Integer.valueOf(graphics.getAttributeValue("width"));
    height = Integer.valueOf(graphics.getAttributeValue("height"));
  }

	/** Returns the id of the entry.
	 *
	 *  @return the id of the entry.
	 */
	public Integer getId() {
		return id;
	}

	/** Returns the name of the entry.
	 *
	 *  @return the name of the entry.
	 */
	public String getName() {
		return name;
	}

	/** Returns the type of the entry.
   *
   *  @return the type of the entry.
	 */
	public Type getType() {
		return type;
	}

	/** Returns the link to the entry.
   *
   *  @return the link to the entry.
	 */
	public String getLink() {
		return link;
	}

	/** Returns the optional value of reaction.
   *
   *  @return the optional value of reaction.
   */
	public Optional<String> getReaction() {
		return Optional.ofNullable(reaction);
	}

  /** Returns the x-coordinate defined in
   *  the graphics attributes.
   *
   *  @return the x-coordinate defined in
   *          the graphics  attributes.
   */
  public Integer getX() {
    return x;
  }

  /** Returns the y-coordinate defined in
   *  the graphics attributes.
   *
   *  @return the y-coordinate defined in
   *          the graphics attributes.
   */
  public Integer getY() {
    return y;
  }

  /** Returns the width defined in
   *  the graphics attributes.
   *
   *  @return the height defined in
   *          the graphics attributes.
   */
  public Integer getWidth() {
    return width;
  }

  /** Returns the height defined in
   *  the graphics attributes.
   *
   *  @return the height defined in
   *          the graphics attributes.
   */
  public Integer getHeight() {
    return height;
  }
}
