package fr.upsud.bibs.rdml.kegg;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import org.jdom2.Element;

public class Relation{

  /** A relation type.
   */
  public enum Type {
    EnzymeEnzyme, ProteinProtein,
    GeneExpression, ProteinCompound,
    Maplink;
  };

  /** The first entry of the relation.
   */
  protected Entry entry1;

  /** The second entry of the relation.
   */
  protected Entry entry2;

  /** The type of the relation
   */
  protected Type type;

  /** The subtypes of the relation
   */
  protected Collection<Subtype> subtypes;

  /** Create a new relation from a KGML "relation"
   *  element.
   *
   *  @param element a KGML "relation" element.
   *  @param entries a map of all the entries defined
   *                 in the KGML file, referenced by
   *                 their KEGG id.
   */
  public Relation(Element element, Map<Integer, Entry> entries) {
    Objects.requireNonNull(element);
    Objects.requireNonNull(entries);

    int entry1_id = Integer.parseInt(element.getAttributeValue("entry1"));
    int entry2_id = Integer.parseInt(element.getAttributeValue("entry2"));

    entry1 = entries.get(entry1_id);
    entry2 = entries.get(entry2_id);

    switch (element.getAttributeValue("type")) {
      case "ECrel": type = Type.EnzymeEnzyme; break;
      case "PPrel": type = Type.ProteinProtein; break;
      case "GErel": type = Type.GeneExpression; break;
      case "PCrel": type = Type.ProteinCompound; break;
      case "maplink": type = Type.Maplink; break;
      default:
        System.err.println("Unknown relation type: "+element.getAttributeValue("type"));
    }

    subtypes = new LinkedList<Subtype>();
    for (Element child: element.getChildren("subtype")) {
      subtypes.add(new Subtype(child));
    }
  }

  /** Returns the type of the relation.
   *
   *  @return the type of the relation.
   */
  public Type getType() {
    return type;
  }

}
