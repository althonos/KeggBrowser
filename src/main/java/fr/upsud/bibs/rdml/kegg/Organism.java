package fr.upsud.bibs.rdml.kegg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Organism implements Comparable<Organism> {

  private String id;
  private String name;
  private String classification;
  private Integer tNumber;

  private static Map<String, Organism> instances = new HashMap<>();


  /** Create an Organism object from its KEGG id.
   *
   *  Since all Organism instances are referenced when
   *  they are created, attempting to create an instance
   *  with the same id as a previously created instance
   *  will actually return that instance instead of
   *  creating a new one.
   *
   * @param id the KEGG id of the organism to
   *           create.
   * @return an Organism object or null if any
   *         error happened while trying to load
   *         the organism.
   */
  public static Organism fromId(String id) {

    Organism org;
    String[] parts;
    String line;
    BufferedReader in;

    if (instances.containsKey(id))
      return instances.get(id);

    in = new BufferedReader(new InputStreamReader(
      Organism.class.getResourceAsStream("organisms.tsv")));

    try {
      while((line = in.readLine()) != null) {
        parts = line.split("\t", 4);
        if (parts[1].equals(id)) {

          org = new Organism(
            Integer.parseInt(parts[0].substring(1)),
            id, parts[2], parts[3]
          );

          instances.put(id, org);

          in.close();

          return org;
        };
      }
      in.close();
    } catch (IOException e) {}

    return null;
  }

  public Organism(Integer tNumber, String id, String name, String classification) {
    this.id = id;
    this.name = name;
    this.tNumber = tNumber;
    this.classification = classification;
  }


  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getClassification() {
    return classification;
  }

  public Integer getTNumber() {
    return tNumber;
  }

  public boolean equals(Object other) {
    if (this.getClass() != other.getClass())
      return false;

    Organism other_org = (Organism) other;

    return this.getName().equals(other_org.getName())
        && this.getId().equals(other_org.getId())
        && this.getTNumber().equals(other_org.getTNumber())
        && this.getClassification().equals(other_org.getClassification());
  }

  public String toString() {
    return String.format("%s <%s>", name, id);
  }

  public int compareTo(Organism other) {
    if (this.equals(other)) return 0;
    else return this.getId().compareTo(other.getId());
  }

}
