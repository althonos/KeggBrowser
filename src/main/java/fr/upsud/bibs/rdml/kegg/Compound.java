package fr.upsud.bibs.rdml.kegg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import org.jdom2.Element;

/** A chemical compound as defined by KEGG.
 *
 *  @see <a href="http://www.kegg.jp/kegg/xml/docs/">KGML
 *  format specification</a>
 */
public class Compound{

  /** The KEGG id of the compound;
   */
  protected Integer id;

  /** The name of the compound.
   *
   *  Format is cpd:CXXXXX where X is
   *  a digit.
   */
  protected String name;

  /** An alternative name for the compound.
   */
  protected List<String> altNames;



  /** Get the alternative names of a Compound from
   *  its KEGG name.
   *
   *  @param name the name of the compound
   *              (cpd:XXXXX)
   *  @return a list of the alternative names of
   *          the compound
   */
  private static List<String> getAltNamesFromName(String name) {
    String[] parts;
    String line;
    BufferedReader in;
    List<String> altNames = new LinkedList<String>();

    in = new BufferedReader(new InputStreamReader(
      Compound.class.getResourceAsStream("compounds.tsv")));

    try {
      while((line = in.readLine()) != null) {
        parts = line.split("\t", 2);
        if (parts[0].equals(name)) {
          altNames.addAll(Arrays.asList(parts[1].split("; ")));
          break;
        }
      }
      in.close();
    } catch (IOException e) {}

    return altNames;
  }

  /** Create a new compound from its id and name.
   *
   *  @param id   the KEGG id of the compound
   *  @param name the name of the compound
   */
  public Compound(Integer id, String name) {
    this(id, name, null);
  }

  /** Create a new compound from its id and name.
   *
   *  @param id   the KEGG id of the compound
   *  @param name the name of the compound
   */
   public Compound(int id, String name) {
    this(Integer.valueOf(id), name, null);
  }

  /** Create a new compound with an alternative name.
   *
   *  @param id       the KEGG id of the compound
   *  @param name     the name of the compound
   *  @param altNames the alternative names of the
   *                  compound
   */
  public Compound(Integer id, String name, List<String> altNames) {
    Objects.requireNonNull(name);

    this.id = id;
    this.name = name;
    this.altNames = getAltNamesFromName(name);
  }

  /** Create a new compound with an alternative name.
   *
   *  @param id       the KEGG id of the compound
   *  @param name     the name of the compound
   *  @param altNames the alternative names of the
   *                  compound
   */
  public Compound(int id, String name, List<String> altNames) {
    this(Integer.valueOf(id), name, altNames);
  }

  /** Create a new compound from a KGML element.
   *
   * @param element a Compound element defined
   *                in a KGML file (either substrate
   *                or product).
   */
  public Compound(Element element) {
    Objects.requireNonNull(element);

    id = Integer.parseInt(element.getAttributeValue("id"));
    name = element.getAttributeValue("name").split(" ", 2)[0];

    // ML: FIXME: Currently always empty, but should
    // check if the element has any alt_name child.
    altNames = new LinkedList<String>();
  };


  /** Returns the id of the compound.
   *
   *  @return the id of the compound
   */
  public Integer getId() {
    return id;
  }

  /** Returns the name of the compound.
   *
   *  @return the name of the compound
   */
  public String getName() {
    return name;
  }

  /** Return an URL to the KEGG.jp page
   *  for this compound.
   *
   *  @return the URL to the KEGG.jp page
   *          for this compound.
   */
  public URL getURL() {
    URL url = null;
    try {
      url = new URL("http", "www.genome.jp", "/dbget-bin/www_bget?"+name);
    } catch (MalformedURLException e) {e.printStackTrace();}
    return url;
  }

  /** Returns the alternative name of the compound.
   *
   *  @return the id of the compound
   */
  public List<String> getAltNames() {
    return altNames;
  }

}
