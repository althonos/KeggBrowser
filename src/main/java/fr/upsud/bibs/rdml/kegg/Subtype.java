package fr.upsud.bibs.rdml.kegg;

import java.util.Objects;
import org.jdom2.Element;

/** A subtype as defined by KEGG
 */
public class Subtype {

  // public enum SubtypeName {
  //   Compound, HiddenCompound, Activation,
  //   Inhibition, Expression, Repression,
  //   IndirectEffect, StateChange, Association,
  //   Dissociation, MissingInteraction,
  //   Phosphorylation, Dephosphorylation,
  //   Glycosylation, Ubiquitination, Methylation,
  // }

  /** The name of the subtype
   */
  protected String name;

  /** The value of the subtype
   */
  protected String value;

  /** Create a new subtype from a KGML element.
   *
   * @param element a "subtype" element defined
   *                in a KGML file.
   */
  public Subtype(Element element) {
    Objects.requireNonNull(element);
    name = element.getAttributeValue("name");
    value = element.getAttributeValue("value");
  }

  /** Create a new subtype from scratch
   *
   *  @param name the name of the subtype
   *  @param value the value of the subtype
   */
  public Subtype(String name, String value) {
    this.name = name;
    this.value = value;
  }

	/** Returns the name of the subtype.
	 *
	 *  @return the name of the subtype.
	 */
	public String getName() {
		return name;
	}

	/** Returns the value of the subtype.
	 *
	 *  @return the value of the subtype.
	 */
	public String getValue() {
		return value;
	}

}
