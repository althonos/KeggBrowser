package fr.upsud.bibs.rdml.kegg;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import org.jdom2.Element;
import org.jdom2.JDOMException;

/** A pathway as defined in KEGG,
 *  for only one organism.
 */
public class SinglePathway extends Pathway {

  protected Organism organism;

  public SinglePathway(Element element) {
    super(element);
  }

  public SinglePathway(File kgmlFile) throws IOException, JDOMException {
    super(kgmlFile);
  }

  public Organism getOrganism() {
    return organism;
  }

  protected void updatePathwayWithElement(Element element) {
    organism = Organism.fromId(element.getAttributeValue("org"));
    super.updatePathwayWithElement(element);
  }

  public Optional<String> getLink() {
    String url;
    url = "http://www.genome.jp/kegg-bin/show_pathway?org_name=";
    url += organism.getId()+String.format("&mapno=%1$05d", number);
    return Optional.of(url);
  }

  public Optional<String> getImage() {
    String url;
    url = "http://www.genome.jp/kegg/pathway/map/";
    url += organism.getId()+String.format("%1$05d.png", number);
    return Optional.of(url);
  }

}
