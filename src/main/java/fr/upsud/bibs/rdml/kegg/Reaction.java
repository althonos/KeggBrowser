package fr.upsud.bibs.rdml.kegg;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Objects;
import org.jdom2.Element;

/** A reaction as defined in a KGML file
 *
 *  @see <a href="http://www.kegg.jp/kegg/xml/docs/">KGML
 *  format specification</a>
 */
public class Reaction {

  /** The KEGG id of the reaction.
   */
  protected Integer id;

  /** The KEGG name of the reaction.
   *
   *  Format is rn:RXXXXX where X is
   *  a digit.
   */
  protected String  name;

  /** The reversibility of the reaction.
   */
  protected boolean reversible;

  /** A list of the substrates of the reaction.
   */
  protected Collection<Compound> substrates;

  /** A list of the products of the reaction.
   */
  protected Collection<Compound> products;

  /** Create a new reaction from scratch.
   *
   *  @param id the KEGG id of the reaction.
   *  @param name the name of the reaction.
   *  @param reversible is the reaction reversible ?
   *  @param substrates the substrates of the reaction.
   *  @param products the products of the reaction.
   */
  public Reaction(Integer id, String name, boolean reversible,
                  Collection<Compound> substrates, Collection<Compound> products) {
    Objects.requireNonNull(name);
    Objects.requireNonNull(substrates);
    Objects.requireNonNull(products);

    this.id = id;
    this.name = name;
    this.substrates = substrates;
    this.products = products;
    this.reversible = reversible;
  }

  /** Create a new reaction from scratch.
   *
   *  @param id the KEGG id of the reaction.
   *  @param name the name of the reaction.
   *  @param reversible is the reaction reversible ?
   *  @param substrates the substrates of the reaction.
   *  @param products the products of the reaction.
   */
  public Reaction(int id, String name, boolean reversible,
                  Collection<Compound> substrates, Collection<Compound> products) {
    this(Integer.valueOf(id), name, reversible, substrates, products);
  }

  /** Create a new reaction from a KGML "reaction"
   *  element.
   *
   *  @param element a KGML "reaction" element.
   */
   public Reaction(Element element) {
     Objects.requireNonNull(element);

     // Parse the attributes
     id = Integer.valueOf(element.getAttributeValue("id"));
     name = element.getAttributeValue("name").split(" ", 2)[0];
     reversible = "reversible".equals(element.getAttributeValue("type"));

     // Init the compound lists
     substrates = new LinkedList<>();
     products = new LinkedList<>();

     // temporary compound that will be created
     // for each child element
     Compound compound;

     // iterate through the children
     for (Element child: element.getChildren()) {

       // Create a new Compound from given KGML element
       compound = new Compound(child);

       // Add the compound to the right list
       switch (child.getName()) {
         case "substrate": this.substrates.add(compound); break;
         case "product":   this.products.add(compound);   break;
         default:
           System.err.println("Unknown compound type: "+child.getName());
       }
     };

   };

   /** Returns the id of the reaction.
    *
    *  @return the id of the reaction.
    */
   public Integer getId() {
     return id;
   }

   /** Returns the name of the reaction.
    *
    *  @return the name of the reaction.
    */
   public String getName() {
     return name;
   }

   /** Returns true if the reaction is reversible.
    *
    *  @return the reversibility of the reaction.
    */
   public boolean isReversible() {
     return reversible;
   }

   /** Returns the list of substrates.
    *
    *  @return the list of substrates.
    */
   public Collection<Compound> getSubstrates() {
     return substrates;
   }

   /** Returns the list of products.
    *
    *  @return the list of substrates
    */
   public Collection<Compound> getProducts() {
     return products;
   }

   /** Return an URL to the KEGG.jp page
    *  for this reaction.
    *
    *  @return the URL to the KEGG.jp page
    *          for this reaction.
    */
  public URL getURL() {
    URL url = null;
    try {
      url = new URL("http", "www.genome.jp", "/dbget-bin/www_bget?"+name);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
    return url;
  }

}
