package fr.upsud.bibs.rdml.kegg;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;

/** A generic pathway, created by combining
 *  several individual Pathway objects.
 */
public class GenericPathway extends Pathway {

  protected List<Organism> organisms;

  /** Create an empty GenericPathway to be extended later on.
   */
  public GenericPathway() {
    super();
    organisms = new LinkedList<Organism>();
  }

  /** Create an empty GenericPathway with an id.
   *
   *  @param id the KEGG id of the pathway
   */
   public GenericPathway(Integer id) {
     this();
     number = id;

     title = getTitleFromTable(id);
   }

  /** Create a GenericPathway from a Pathway object.
   *
   *  @param pathway the pathway to clone
   */
  public GenericPathway(SinglePathway pathway) {
    this();
    Objects.requireNonNull(pathway);
    extend(pathway);
  }

  /** Create a GenericPathway from a KGML file.
   *
   *  @param kgmlFile the file containing a KEGG pathway.
   *  @throws IOException when the file cannot be opened.
   *  @throws JDOMException when the DOM parser fails.
   */
  public GenericPathway(File kgmlFile) throws IOException, JDOMException {
    Objects.requireNonNull(kgmlFile);

    Document document = _sxb.build(kgmlFile);
    updatePathwayWithElement(document.getRootElement());
  }

  /** Update the pathway with data contained
   *  in the "pathway" element.
   *
   *  @param element a KGML "pathway" element.
   */
  protected void updatePathwayWithElement(Element element) {
    Objects.requireNonNull(element);

    organisms.add(Organism.fromId(element.getAttributeValue("org")));
    super.updatePathwayWithElement(element);

    number = Integer.valueOf(element.getAttributeValue("number"));
    name   = String.format("path:map%1$05d", number);
  }

  /** Extend the GenericPathway with another pathway
   *
   *  This method is thread-safe, as a lock is acquired
   *  before the GenericPathway is modified and released
   *  afterwards.
   *
   *  @param pathway the pathway to extend the generic
   *                 pathway with (adds the reactions,
   *                 the relations, the entries and
   *                 the organisms to the generic
   *                 pathway)
   */
  public void extend(SinglePathway pathway) {
    Objects.requireNonNull(pathway);

    lock();

    if (title == null) {
      title  = pathway.title;
      number = pathway.number;
      name   = String.format("path:map%1$05d", number);
    }

    organisms.add(pathway.getOrganism());

    for (Entry entry: pathway.getEntries())
      addEntry(entry);
    for (Relation relation: pathway.relations)
      addRelation(relation);
    for (Reaction reaction: pathway.getReactions())
      addReaction(reaction);

    unlock();
  }

  public List<Organism> getOrganisms() {
    return organisms;
  }

  public Organism getOrganism(Integer index) {
    if (index >= organisms.size()) throw new IndexOutOfBoundsException();
    return organisms.get(index);
  }

  /** Returns the image of the Pathway.
   *
   *  @return the image of the Pathway.
   */
  public Optional<String> getImage() {
    String url = null;
    if (number != null)
      url = "http://www.kegg.jp/kegg/pathway/map/map"+number+".png";
    return Optional.ofNullable(url);
  }

  /** Returns a link to the Pathway.
   *
   *  @return a link to the Pathway.
   */
  public Optional<String> getLink() {
    String url = null;
    if (number != null)
      url = "http://www.kegg.jp/kegg-bin/show_pathway?org_name=map&mapno="+number;
    return Optional.ofNullable(url);
  }

  /** Add a reaction to the generic pathway.
   *
   *  @param reaction the reaction to add.
   */
  public void addReaction(Reaction reaction) {
    Objects.requireNonNull(reaction);
    reactions.put(reaction.getId(), reaction);

    graph.addReaction(reaction, organisms.get(organisms.size()-1));
  }

  /** Get the title of a Pathway corresponding to
   *  a given ID from the KEGG pathways table.
   *
   *  @param id a KEGG pathway id.
   *  @return the title of the pathway corresponding
   *          to the given id.
   */
  private static String getTitleFromTable(Integer id) {
    BufferedReader in;
    String line;
    String[] parts;

    in = new BufferedReader(new InputStreamReader(
      GenericPathway.class.getResourceAsStream("pathways.tsv")));

    String fullId = String.format("path:map%1$05d", id);

    try {
      while((line = in.readLine()) != null) {
        parts = line.split("\t", 2);
        if (parts[0].equals(fullId)) {
          in.close();
          return parts[1];
        };
      }
      in.close();
    } catch (IOException e) {}

    return null;
  }

  /** Renders the Pathway as a String.
   *
   *  @return a string with the KEGG ID and the
   *          title of the Pathway if any.
   */
  public String toString() {
    return String.format("<path:map%1$05d> ", number)
         + (title == null ? "" : title);
  }

}
