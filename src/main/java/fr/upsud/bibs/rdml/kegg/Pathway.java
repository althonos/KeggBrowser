package fr.upsud.bibs.rdml.kegg;

import fr.upsud.bibs.rdml.graph.PathwayGraph;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/** An abstract pathway.
 */
public abstract class Pathway {

  /** The name of the Pathway.
   *
   *  Format is always path:orgXXXXX, where <i>org</i> is a
   *  3-or-4-letters-long organism ID from
   *  <a href="http://www.genome.jp/kegg/catalog/org_list.html">
   *  KEGG Organisms</a>.
   */
  protected String name;

  /** The id of the Pathway.
   */
  protected Integer number;

  /** The title of the Pathway (natural language).
   */
  protected String title;

  /** The organisms of this pathway.
   *
   *  @implNote for consistency with GenericPathway objects,
   *            this attribute is a list even for single
   *            pathways. As such, Pathway will always have
   *            only one organism, wrapped in a Collection.
   */
  //protected List<Organism> organisms;

  /** The entries of this pathway,
   *  referenced by their KEGG id.
   */
  protected Map<Integer, Entry> entries;

  /** The reactions of this pathway,
   *  referenced by their KEGG id.
   */
  protected Map<Integer, Reaction> reactions;

  /** The relations of this pathway.
   */
  protected List<Relation> relations;

  /** The graph representation for this pathway.
   */
  protected PathwayGraph graph;

  /** A mutex preventing concurrent modifications
   *  of the Pathway object.
   */
  private final Lock _mutex = new ReentrantLock();

  /** A shared SAX Builder.
   */
  static protected SAXBuilder _sxb     = new SAXBuilder();

  /** Prevent the SAXBuilder from loading the external
   *  DTD file.
   */
  static {
    _sxb.setFeature(
      "http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
  }

  /** Create an empty Pathway.
   */
  protected Pathway() {
    //organisms = new LinkedList<Organism>();
    entries   = new HashMap<Integer, Entry>();
    reactions = new HashMap<Integer, Reaction>();
    relations = new LinkedList<Relation>();

    graph = new PathwayGraph(this);
  }

  /** Create a Pathway from a KGML file.
   *
   *  @param kgmlFile a KGML file.
   *  @throws IOException when the file cannot be opened.
   *  @throws JDOMException when the JDOM parser fails to
   *                        parse the file.
   */
  public Pathway(File kgmlFile) throws IOException, JDOMException {
    this();
    Objects.requireNonNull(kgmlFile);

    Document document = _sxb.build(kgmlFile);
    updatePathwayWithElement(document.getRootElement());
  }

  /** Create a Pathway from a KGML "pathway"
   *  element.
   *
   *  @param element a KGML "pathway" element.
   */
  public Pathway(Element element) {
    this();
    Objects.requireNonNull(element);
    updatePathwayWithElement(element);
  }

  /** Update the pathway with data contained
   *  in the "pathway" element.
   *
   *  @param element a KGML "pathway" element.
   */
  protected void updatePathwayWithElement(Element element) {
    Objects.requireNonNull(element);
    lock();

    name   = element.getAttributeValue("name");
    number = Integer.valueOf(element.getAttributeValue("number"));
    title  = element.getAttributeValue("title");
    // organisms.add(Organism.fromId(element.getAttributeValue("org")));

    for (Element child: element.getChildren("entry"))
      addEntry(new Entry(child));
    for (Element child: element.getChildren("reaction"))
      addReaction(new Reaction(child));
    for (Element child: element.getChildren("relation"))
      addRelation(new Relation(child, entries));

    unlock();
  }

  /** Lock the Pathway.
   */
  protected void lock() {
    _mutex.lock();
  }

  /** Unlock the Pathway.
   */
  protected void unlock() {
    _mutex.unlock();
  }

  /** Returns the name of the Pathway.
   *
   *  @return the name of the Pathway.
   */
  public String getName() {
    return name;
  }

  /** Returns the number of the Pathway.
   *
   *  @return the number of the Pathway.
   */
  public Integer getNumber() {
    return number;
  }

  // /** Returns the organisms of the Pathway.
  //  *
  //  *  @return the organisms of the Pathway.
  //  */
  // public List<Organism> getOrganisms() {
  //   return organisms;
  // }

  /** Returns the image of the Pathway.
   *
   *  @return the image of the Pathway.
   */
   public abstract Optional<String> getImage();

  /** Returns a link to the Pathway.
   *
   *  @return a link to the Pathway.
   */
   public abstract Optional<String> getLink();


  /** Returns the graph of the Pathway.
   *
   *  @return the graph of the Pathway.
   */
  public PathwayGraph getGraph() {
    return graph;
  }

  /** Returns the title of the Pathway.
   *
   *  @return the title of the Pathway.
   */
  public String getTitle() {
    return title;
  }

  /** Returns relations defined in the Pathway.
   *
   *  @return relations defined in the Pathway.
   */
  public List<Relation> getRelations() {
    return relations;
  }

  /** Returns entries defined in the Pathway.
   *
   *  @return entries defined in the Pathway.
   */
  public Collection<Entry> getEntries() {
    return entries.values();
  }

  /** Returns reactions defined in the Pathway.
   *
   *  @return relations defined in the Pathway.
   */
  public Collection<Reaction> getReactions() {
    return reactions.values();
  }

  /** Returns the entry corresponding to a
   *  given KEGG id.
   *
   *  @param id the id of the entry.
   *  @return the entry corresponding to the
   *          given KEGG id.
   */
  public Entry getEntry(Integer id) {
    Objects.requireNonNull(id);
    return entries.get(id);
  }

  /** Returns the reaction corresponding to
   *  a given KEGG id.
   *
   *  @param id the id of the reaction.
   *  @return the reaction corresponding to the
   *          given KEGG id.
   */
  public Reaction getReaction(Integer id) {
    Objects.requireNonNull(id);
    return reactions.get(id);
  }

  /** Returns the reaction corresponding
   *  to a given name.
   *
   *  @param name the name of the reaction
   *              (format is rn:RXXXXX)
   *  @return the reaction corresponding to
   *          the given name.
   */
  public Reaction getReaction(String name) {
    for (Reaction r: reactions.values())
      if (r.getName().equals(name))
        return r;
    return null;
  }

  /** Returns the Compound corresponding
   *  to a given name.
   *
   *  @param name the name of the compound.
   *              (format is cpd:CXXXXX)
   *  @return the compound corresponding to
   *          the given name.
   */
  public Compound getCompound(String name) {
    for (Reaction reaction : reactions.values()) {
      for (Compound compound : reaction.getProducts())
        if (compound.getName().equals(name))
          return compound;
      for (Compound compound : reaction.getSubstrates())
        if (compound.getName().equals(name))
          return compound;
    }
    return null;
  }


  /** Add an entry to the Pathway.
   *
   *  @param entry the entry to add.
   */
  protected void addEntry(Entry entry) {
    Objects.requireNonNull(entry);
    entries.put(entry.getId(), entry);
  }

  /** Add an reaction to the Pathway.
   *
   *  @param reaction the reaction to add.
   */
  protected void addReaction(Reaction reaction) {
    Objects.requireNonNull(reaction);
    reactions.put(reaction.getId(), reaction);

    graph.addReaction(reaction);
  }

  /** Add an relation to the Pathway.
   *
   *  @param relation the relation to add.
   */
  protected void addRelation(Relation relation) {
    Objects.requireNonNull(relation);
    relations.add(relation);
  }

}
