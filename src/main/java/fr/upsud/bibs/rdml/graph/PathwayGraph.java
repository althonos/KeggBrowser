package fr.upsud.bibs.rdml.graph;

import fr.upsud.bibs.rdml.kegg.Compound;
import fr.upsud.bibs.rdml.kegg.Entry;
import fr.upsud.bibs.rdml.kegg.GenericPathway;
import fr.upsud.bibs.rdml.kegg.Organism;
import fr.upsud.bibs.rdml.kegg.Pathway;
import fr.upsud.bibs.rdml.kegg.Reaction;
import fr.upsud.bibs.rdml.kegg.SinglePathway;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import org.graphstream.graph.Element;
import org.graphstream.graph.IdAlreadyInUseException;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.stream.file.FileSinkImages;

/** A graphstream Graph specialised in
 *  rendering metabolic pathways.
 */
public class PathwayGraph extends MultiGraph {

  /** The pathway to render.
   */
  private Pathway pathway;

  /** The stylesheet to use.
   */
  private String stylesheet;

  /** A counter of created graph, to
   *  avoid Graph name collision.
   */
  static private Integer graph_id = 0;

  /** Display state of Compound labels.
   */
  private boolean displayCompoundLabels;

  /** Display state of Reaction labels.
   */
  private boolean displayReactionLabels;

  /**

  /** Create a new PathwayGraph without
   *  any associated Pathway.
   */
  public PathwayGraph() {
    super(String.valueOf(++graph_id));
    displayCompoundLabels = true;
    displayReactionLabels = true;
    setDefaultStylesheet();
    addAttribute("ui.antialising");
    addAttribute("ui.quality");
  }

  /** Create a new PathwayGraph with
   *  an associated Pathway.
   *
   *  @param pathway the pathway to render.
   */
  public PathwayGraph(Pathway pathway) {
    this();
    this.pathway = pathway;
  }

  /** Set the default stylesheet.
   */
  public void setDefaultStylesheet() {
    setStylesheet(Pathway.class.getResource("/stylesheets/graph.css"));
  }

  /** Set a new stylesheet from a String.
   *
   *  @param stylesheet the new stylesheet.
   */
  public void setStylesheet(String stylesheet) {
    removeAttribute("ui.stylesheet");
    this.stylesheet = "";
    updateStylesheet(stylesheet);
  }

  /** Update the current stylesheet with a String.
   *
   *  @param stylesheet a stylesheet to update
   *                    the current stylesheet with.
   */
  public void updateStylesheet(String stylesheet) {
    this.stylesheet += stylesheet;
    addAttribute("ui.stylesheet", stylesheet);
  }

  /** Set the new stylesheet from a File.
   *
   * @param stylesheet the file containing the
   *                   new stylesheet.
   */
  public void setStylesheet(File stylesheet) {
    removeAttribute("ui.stylesheet");
    this.stylesheet = "";
    updateStylesheet(stylesheet);
  }

  /** Update the stylesheet from a File.
   *
   *  @param stylesheet the file containing the
   *                    stylesheet to update the
   *                    current stylesheet with.
   */
  public void updateStylesheet(File stylesheet) {
    try {
      updateStylesheet(stylesheet.toURI().toURL());
    } catch(MalformedURLException e) {}
  }

  /** Set the stylesheet from an URL.
   *
   *  @param stylesheet an URL to the file
   *                    containing the new
   *                    stylesheet.
   */
  public void setStylesheet(URL stylesheet) {
    removeAttribute("ui.stylesheet");
    this.stylesheet = "";
    updateStylesheet(stylesheet);
  }

  /** Update the stylesheet from an URL.
  *
  *  @param stylesheet an URL to the file
  *                    containing the stylesheet
  *                    to update the current
  *                    stylesheet with.
  */
  public void updateStylesheet(URL stylesheet) {
    this.stylesheet += String.format("url(%s)", stylesheet);
    addAttribute("ui.stylesheet", this.stylesheet);
  }

  /** Update the graph stylesheet with the
   *  cached stylesheet.
   */
  private void updateStylesheet() {
    addAttribute("ui.stylesheet", this.stylesheet);
  }

  /** Returns the stylesheet of the graph.
   *
   *  @return the stylesheet of the graph.
   */
  public String getStylesheet() {
    return stylesheet;
  }

  /** Clear the cached and used stylesheets.
   */
  public void clearStylesheet() {
    removeAttribute("ui.stylesheet");
    stylesheet = "";
  }

  /** Add a reaction to the Graph.
   *
   *  @param reaction the reaction to add
   *                  to the Graph.
   */
  public void addReaction(Reaction reaction) {
    addReactionNode(reaction);

    for (Compound substrate: reaction.getSubstrates())
      addSubstrateNode(substrate, reaction);
    for (Compound product: reaction.getProducts())
      addProductNode(product, reaction);
  }

  /** Add a reaction to the Graph with a tagged organism.
   *
   *  @param organism the organism the reaction comes from.
   *  @param reaction the reaction to add to the graph.
   */
  public void addReaction(Reaction reaction, Organism organism) {
    Node temp;

    temp = addReactionNode(reaction);
    addOrganismToElement(temp, organism);

    for (Compound substrate: reaction.getSubstrates()) {
      temp = addSubstrateNode(substrate, reaction);
      addOrganismToElement(temp, organism);
    }
    for (Compound product: reaction.getProducts()) {
      temp = addProductNode(product, reaction);
      addOrganismToElement(temp, organism);
    }
  }

  /** Add a substrate node.
   *
   *  @param substrate the substrate to add
   *  @param reaction the reaction the substrate if part of.
   *  @return the freshly created node.
   */
  private Node addSubstrateNode(Compound substrate, Reaction reaction) {
    Objects.requireNonNull(substrate);
    Objects.requireNonNull(reaction);

    String sid = String.valueOf(substrate.getId());
    String rid = String.valueOf(reaction.getId());

    Node node;
    if ((node = getNode(sid)) == null)
      node = addNode(sid);

    try {
      addEdge(sid+"->"+rid, sid, rid, true);
    } catch(IdAlreadyInUseException e) {}

    if (reaction.isReversible())
      try {
        addEdge(rid+"->"+sid, rid, sid, true);
      } catch(IdAlreadyInUseException e) {}


    node.setAttribute("ui.class", "compound");
    node.setAttribute("ui.label", substrate.getName());

    Entry entry = pathway.getEntry(substrate.getId());
    node.setAttribute("xy", entry.getX(), -entry.getY());

    return node;
  }

  /** Add a product node.
   *
   *  @param substrate the product to add.
   *  @param reaction the reaction the product if part of.
   *  @return the freshly created node.
   */
  private Node addProductNode(Compound product, Reaction reaction) {
    Objects.requireNonNull(product);
    Objects.requireNonNull(reaction);

    Node node;
    String pid = String.valueOf(product.getId());
    String rid = String.valueOf(reaction.getId());

    if ((node = getNode(pid)) == null)
      node = addNode(pid);

    try {
      addEdge(rid+"->"+pid, rid, pid, true);
    } catch(IdAlreadyInUseException e) {}

    if (reaction.isReversible())
      try {
        addEdge(pid+"->"+rid, pid, rid, true);
      } catch(IdAlreadyInUseException e) {}

    node.setAttribute("ui.class", "compound");
    node.setAttribute("ui.label", product.getName());

    Entry entry = pathway.getEntry(product.getId());
    node.setAttribute("xy", entry.getX(), -entry.getY());

    return node;
  }

  /** Set whether or not to display Compound labels.
   *
   *  @param on true if label must be displayed.
   */
  public void showCompoundLabels(boolean on) {
    displayCompoundLabels = on;
    if (on)
      updateStylesheet("node.compound {shape: circle; text-mode: normal;}");
    else
      updateStylesheet("node.compound {shape: circle; text-mode: hidden;}");
  }

  /** Toggle Compound labels display.
   */
  public void toggleCompoundLabels() {
    showCompoundLabels(!displayCompoundLabels);
  }

  /** Set whether or not to display Compound labels.
   *
   *  @param on true if label must be displayed.
   */
  public void showReactionLabels(boolean on) {
    displayReactionLabels = on;
    if (on)
      updateStylesheet("node.reaction {shape: box; text-mode: normal;}");
    else
      updateStylesheet("node.reaction {shape: box; text-mode: hidden;}");
  }

  /** Toggle Compound labels display.
   */
  public void toggleReactionLabels() {
    showReactionLabels(!displayReactionLabels);
  }

  /** Add a reaction node.
   *
   *  @param reaction the reaction to add.
   *  @return the freshly created node.
   */
  private Node addReactionNode(Reaction reaction) {
    Objects.requireNonNull(reaction);

    String rid = String.valueOf(reaction.getId());

    Node node;
    if ((node = getNode(rid)) == null)
      node = addNode(rid);

    node.setAttribute("ui.class", "reaction");
    node.setAttribute("ui.label", reaction.getName());
    node.setAttribute("url", reaction.getURL());

    Entry entry = pathway.getEntry(reaction.getId());
    node.setAttribute("xy", entry.getX(), -entry.getY());

    return node;
  }


  /** Called before running a coloring operation.
   */
  private void beforeColoring() {
    clearStylesheet();
  }

  /** Called after running a coloring operation.
   */
  private void afterColoring() {
    stylesheet += "\n node {text-alignment: under; size: 10px; stroke-mode: plain; ";
    stylesheet += "stroke-color: black; stroke-width: 1px; text-alignment: under;}";
    showCompoundLabels(displayCompoundLabels);
    showReactionLabels(displayReactionLabels);
    updateStylesheet();
  }

  /** Color nodes according to given colors depending
   *  on the organism where they are present.
   *
   *  @param organism the organism to use
   *  @param in the color with which to paint compound Nodes
   *            or reaction Nodes present from the given Organism.
   *  @param out the color with which to paint compound Nodes
   *            or reaction Nodes missing from the given Organism.
   */
  public void colorOrganism(Organism organism, Color in, Color out) {
    Set<String> organisms;

    beforeColoring();

    for(Node node : getEachNode()) {
      organisms = node.getAttribute("organisms");
      if (organisms.contains(organism.getId())) colorNode(node, in);
      else colorNode(node, out);
    }

    afterColoring();
  }

  /** Color nodes according to their conservation
   *  (the ratio of organisms sharing the
   *  compound / the reaction on the total number
   *  of organisms).
   *
   *  @param min the color of the nodes with the lowest
   *             conservation ratio (only present in one
   *             organism).
   *  @param max the color of the nodes with the highest
   *             conservation ratio (only present in one
   *             organism).
   */
  public void colorConservation(Color min, Color max) {
    Set<String> organisms;
    int totalOrganisms;
    float proportion;
    Color newColor;

    beforeColoring();

    if (pathway instanceof SinglePathway) {
      for (Node node : getEachNode())
        colorNode(node, max);

    } else {
      totalOrganisms = ((GenericPathway) pathway).getOrganisms().size();
      for (Node node : getEachNode()) {
        organisms = node.getAttribute("organisms");
        proportion = (float) organisms.size() / totalOrganisms;
        newColor = new Color(
          (int) (max.getRed()  *proportion + min.getRed()  *(1-proportion)),
          (int) (max.getGreen()*proportion + min.getGreen()*(1-proportion)),
          (int) (max.getBlue() *proportion + min.getBlue() *(1-proportion))
        );
        colorNode(node, newColor);
      }
    }

    afterColoring();
  }

  /** Color reactions with one color and compounds
   *  with another.
   *
   *  @param reaction the color to paint reaction nodes with.
   *  @param compound the color to paint compound nodes with.
   */
  public void colorDefault(Color reaction, Color compound) {
    String nodeClass;

    beforeColoring();

    for (Node node : getEachNode()) {
      nodeClass = node.getAttribute("ui.class");
      if (nodeClass.equals("compound"))
        colorNode(node, compound);
      else
        colorNode(node, reaction);
    }

    afterColoring();
  }

  /** Create a screenshot of the graph and export it
   *  to a file.
   *
   *  @param outputFile the file in which to export
   *                    the PNG screenshot.
   */
  public void exportScreenshot(File outputFile) {
    Objects.requireNonNull(outputFile);

    FileSinkImages pic = new FileSinkImages(
     FileSinkImages.OutputType.PNG, FileSinkImages.Resolutions.HD1080);
    pic.setLayoutPolicy(
     FileSinkImages.LayoutPolicy.NO_LAYOUT);
    pic.setQuality(FileSinkImages.Quality.HIGH);
    pic.setRenderer(FileSinkImages.RendererType.SCALA);
    pic.setStyleSheet(stylesheet);

     try {
       pic.writeAll(this, outputFile.getCanonicalPath());
     } catch (IOException e) {}
  }

  /* UTILITY METHODS */
  /** Add an organism to a graphstream Element as an attribute.
   *
   *  @param elem the graphstream element to tag
   *  @param organism the organism with which to tag the element.
   *  @return true if the element was not yet tagged with
   *          the given organism.
   */
  static private boolean addOrganismToElement(Element elem, Organism organism) {
    Set<String> organisms;
    if ((organisms = elem.getAttribute("organisms")) == null) {
      organisms = new HashSet<String>();
      elem.addAttribute("organisms", organisms);
    }
    return organisms.add(organism.getId());
  }

  /** Color a Node with a given Color
   *
   *  @param node the node to color
   *  @param color the color to use
   *
   *  @implNote this doesn't actually color the node
   *            directly, but updated the stylesheet
   *            with node-specific CSS.
   */
  private void colorNode(Node node, Color color) {
    stylesheet += "\n" + String.format(
      "node#'%s' {fill-color: #%06x;}",
      node.getId(),
      color.getRGB() & 0x00FFFFFF
    );
  }

}
