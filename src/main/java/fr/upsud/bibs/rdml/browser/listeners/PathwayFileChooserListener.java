package fr.upsud.bibs.rdml.browser.listeners;

import fr.upsud.bibs.rdml.browser.KeggBrowser;
import fr.upsud.bibs.rdml.kegg.SinglePathway;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/** The listener that handles directory selection.
 *
 *  Pops a JFileChooser when an action is performed,
 *  and update the directory of the KeggBrowser.
 *
 *  @see <a href="https://docs.oracle.com/javase/7/docs/
 *       api/javax/swing/JFileChooser.html">JFileChooser</a>
 */
public class PathwayFileChooserListener implements ActionListener {

    /** The KeggBrowser where this handler
     *  is used.
     */
    private KeggBrowser browser;

    /** Create a new PathwayFileChooserListener.
     *
     *  @param browser a KeggBrowser instance.
     */
    public PathwayFileChooserListener(KeggBrowser browser) {
      this.browser  = browser;
    }

    /** Defines what happens when the button is clicked.
     *
     * @param e the selection event.
     */
    @Override
    public void actionPerformed(ActionEvent e) {

      // Create a file chooser
      JFileChooser chooser = new JFileChooser();
      chooser.setCurrentDirectory(new File("."));
      chooser.setDialogTitle("Select pathway directory...");
      chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
      // disable the "All files" option.
      chooser.addChoosableFileFilter(
        new FileNameExtensionFilter("*.kgml", "kgml"));
      chooser.setAcceptAllFileFilterUsed(false);

      if (chooser.showOpenDialog(browser) == JFileChooser.APPROVE_OPTION) {

        try {
          SinglePathway pathway = new SinglePathway(chooser.getSelectedFile());
          browser.showSinglePathway(pathway);
        } catch (Exception ex) {}
      }

    }

}
