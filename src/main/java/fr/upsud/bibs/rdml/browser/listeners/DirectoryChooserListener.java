package fr.upsud.bibs.rdml.browser.listeners;

import fr.upsud.bibs.rdml.browser.KeggBrowser;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;

/** The listener that handles directory selection.
 *
 *  Pops a JFileChooser when an action is performed,
 *  and update the directory of the KeggBrowser.
 *
 *  @see <a href="https://docs.oracle.com/javase/7/docs/
 *       api/javax/swing/JFileChooser.html">JFileChooser</a>
 */
public class DirectoryChooserListener implements ActionListener {

    /** The KeggBrowser where this handler
     *  is used.
     */
    private KeggBrowser browser;

    /** Create a new DirectoryChooserListener.
     *
     *  @param browser a KeggBrowser instance.
     */
    public DirectoryChooserListener(KeggBrowser browser) {
      this.browser  = browser;
    }

    /** Defines what happens when the button is clicked.
     *
     * @param e the selection event.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      // Create a file chooser
      JFileChooser chooser = new JFileChooser();
      chooser.setCurrentDirectory(new File("."));
      chooser.setDialogTitle("Select pathway directory...");
      chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

      // disable the "All files" option.
      chooser.setAcceptAllFileFilterUsed(false);

      // Get the selected file / directory
      if (chooser.showOpenDialog(browser) == JFileChooser.APPROVE_OPTION)
        browser.setDirectory(
          chooser.getSelectedFile() == null ?
          chooser.getCurrentDirectory() :
          chooser.getSelectedFile()
        );
    }

}
