package fr.upsud.bibs.rdml.browser.listeners;

import fr.upsud.bibs.rdml.browser.KeggBrowser;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/** The listener that handles screenshots.
 *
 *  Pops a JFileChooser when an action is performed,
 *  and attempt to save a screenshot at the given
 *  path.
 *
 *  @see <a href="https://docs.oracle.com/javase/7/docs/api/javax/swing/JFileChooser.html">JFileChooser</a>
 */
public class ScreenshotFileChooserListener implements ActionListener {

    /** The KeggBrowser where this
     *  handler is used.
     */
    private KeggBrowser browser;

    /** A file chooser widget.
     */
    private JFileChooser chooser;

    /** Create a new ScreenshotFileChooserListener.
     *
     *  @param browser a KeggBrowser instance.
     */
    public ScreenshotFileChooserListener(KeggBrowser browser) {
      this.browser  = browser;
      chooser = new JFileChooser();
      chooser.setDialogTitle("Create screenshot...");
      chooser.addChoosableFileFilter(
        new FileNameExtensionFilter("Images", "jpg", "png", "bmp"));
    }

    /** Defines what happens when the screenshot button
     *  is pressed.
     *
     *  @param e an action event.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      if (chooser.showSaveDialog(browser) == JFileChooser.APPROVE_OPTION) {
        File file = chooser.getSelectedFile();
        System.out.println(file);
        String suffix = getSuffix(file);
        if (!("bmp".equals(suffix) || "png".equals(suffix) || "jpg".equals(suffix)))
          file = new File(file.getAbsolutePath()+".png");
        browser.getDisplayer().getGraph().exportScreenshot(file);
      }
    }

    /** Returns the extension of a file.
     *  
     *  @param file a file objects.
     *  @return the extension of the file.
     */
    private String getSuffix(File file) {
        String filestr = file.getPath();
        String suffix = null;
        int i = filestr.lastIndexOf('.');
        if (i > 0 && i < filestr.length())
            suffix = filestr.substring(i + 1).toLowerCase();
        return suffix;
    }

}
