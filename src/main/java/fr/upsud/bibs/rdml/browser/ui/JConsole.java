package fr.upsud.bibs.rdml.browser.ui;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

/** A JLabel mimicking the format of
 *  a console (white monospace text on
 *  black background).
 */
public class JConsole extends JLabel {

  /** The text that's being displayed.
   */
  //private String text;

  /** Create a new JConsole instance.
   */
  public JConsole() {
     super();
     super.setOpaque(true);
     super.setBackground(Color.BLACK);
     info("");
  }

  /** Toggle the visibility of the
   *  console.
   */
  public void toggleVisible() {
    if (super.isVisible())
      super.setVisible(false);
    else
      super.setVisible(true);
  }

  /** Show some text in the JConsole.
   *
   *  @param text the text to show.
   */
  public void info(final String text) {
    //this.text = text;
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        JConsole.super.setText(
          "<html><font face=\"monospace\""
          +" color=\"white\"> &gt; "
          +text+"</font></html>"
        );
      }
    });
  }

}
