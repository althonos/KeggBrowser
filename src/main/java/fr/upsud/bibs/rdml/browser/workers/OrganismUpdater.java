package fr.upsud.bibs.rdml.browser.workers;

import fr.upsud.bibs.rdml.browser.KeggBrowser;
import fr.upsud.bibs.rdml.kegg.GenericPathway;
import fr.upsud.bibs.rdml.kegg.Organism;
import fr.upsud.bibs.rdml.kegg.Pathway;
import fr.upsud.bibs.rdml.kegg.SinglePathway;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import javax.swing.Timer;

/** Timed thread in charge of updating the
 *  organisms in the browser.
 */
public class OrganismUpdater extends Timer implements ActionListener {

  /** A KeggBrowser instance.
   */
  private KeggBrowser browser;

  /** Create a new OrganismUpdater instance.
   *
   *  @param browser a KeggBrowser instance.
   *  @param delay how regularly the listener
   *               is called (in milliseconds).
   */
  public OrganismUpdater(Integer delay, KeggBrowser browser) {
    super(delay, null);
    this.browser = browser;
    addActionListener(this);
    setRepeats(true);
  }

  /** Called regularly by the timer.
   *
   *  Will update the organism list and stop
   *  the thread once all PathwayUpdater threads
   *  are done.
   *
   *  @param evt an action event.
   */
  @Override
  public void actionPerformed(ActionEvent evt) {
    List<Organism> organisms;
    if (PathwayUpdater.getActive().size() == 0) {
      organisms = getOrganisms(browser.getDisplayer().getPathway());
      browser.getOrganismList().setListData(new Vector<Organism>(organisms));
      stop();
    }
  }

  /** Get a list of organisms from a Pathway.
   *
   *  @param pathway the pathway to get organisms from
   *  @return a list of organisms in the pathway.
   */
  public static List<Organism> getOrganisms(Pathway pathway) {
    if (pathway instanceof GenericPathway)
      return getOrganisms((GenericPathway) pathway);
    else if (pathway instanceof SinglePathway)
      return getOrganisms((SinglePathway) pathway);
    else
      return null;
  }

  /** Get a list of organisms from a Pathway.
   *
   *  @param pathway the pathway to get organisms from
   *  @return a list of organisms in the pathway.
   */
  public static List<Organism> getOrganisms(GenericPathway pathway) {
    return pathway.getOrganisms();
  }

  /** Get a list of organisms from a Pathway.
   *
   *  @param pathway the pathway to get organisms from
   *  @return a list of organisms in the pathway.
   */
  public static List<Organism> getOrganisms(SinglePathway pathway) {
    return Arrays.asList(pathway.getOrganism());
  }

}
