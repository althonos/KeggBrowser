package fr.upsud.bibs.rdml.browser.listeners;

import fr.upsud.bibs.rdml.browser.KeggBrowser;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/** The listener that handles pathway selection.
 */
public class MapListSelectionHandler implements ListSelectionListener {

  /** A reference to the KeggBrowser
   */
  private KeggBrowser browser;

  /** Create a new MapListSelectionHandler.
   *
   *  @param browser a KeggBrowser instance.
   */
  public MapListSelectionHandler(KeggBrowser browser) {
    this.browser = browser;
  }

  /** Defines what happens when the selection changes.
   *
   * @param e the selection event.
   */
  @Override
  public void valueChanged(ListSelectionEvent e) {
    ListSelectionModel model = (ListSelectionModel) e.getSource();
    int index = browser.getMapList().getSelectedIndex();
    if (!model.isSelectionEmpty() && e.getValueIsAdjusting())
      browser.loadPathway(browser.getMap(index));
  }
}
