package fr.upsud.bibs.rdml.browser;

import com.esotericsoftware.wildcard.Paths;
import fr.upsud.bibs.rdml.browser.listeners.DirectoryChooserListener;
import fr.upsud.bibs.rdml.browser.listeners.MapListSelectionHandler;
import fr.upsud.bibs.rdml.browser.listeners.OrganismListSelectionHandler;
import fr.upsud.bibs.rdml.browser.listeners.PathwayFileChooserListener;
import fr.upsud.bibs.rdml.browser.listeners.ScreenshotFileChooserListener;
import fr.upsud.bibs.rdml.browser.ui.JConsole;
import fr.upsud.bibs.rdml.browser.ui.KeggBrowserToolBar;
import fr.upsud.bibs.rdml.browser.ui.PathwayDisplayer;
import fr.upsud.bibs.rdml.browser.workers.ColorUpdater;
import fr.upsud.bibs.rdml.browser.workers.OrganismUpdater;
import fr.upsud.bibs.rdml.browser.workers.PathwayUpdater;
import fr.upsud.bibs.rdml.kegg.GenericPathway;
import fr.upsud.bibs.rdml.kegg.Organism;
import fr.upsud.bibs.rdml.kegg.SinglePathway;
import java.awt.*;
import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.*;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.event.*;
import javax.swing.table.*;

/** The main KeggBrowser window.
 */
public final class KeggBrowser extends JFrame {

  /** The currently browsed directory.
   */
  private File directory;

  /** The central PathwayDisplayer panel.
   */
  private PathwayDisplayer displayer;

  /** The panel containing lists on the right
   *  side of the window.
   */
  private JPanel lists;

  /** A list of available generic pathways.
   */
  private List<GenericPathway> maps;
  //
  // /** A list of available organisms.
  //  */
  // private List<Organism> organisms;

  /** The JList rendering the available
   *  generic pathways.
   */
  private JList<String> mapList;

  /** The JList rendering the available
   *  organisms.
   */
  private JList<Organism> organismList;

  /** The menu bar on the top of the window.
   */
  private JMenuBar menuBar;

  /** The toolbar on the left side of the
   *  window.
   */
  private KeggBrowserToolBar toolbar;

  /** The debug console.
   */
  private JConsole console;

  /** Create a new KeggBrowser window.
   */
  public KeggBrowser() {
    initWindow();
    initLists();
    initMenuBar();
    initDisplayer();
    initToolBar();
    initConsole();
    setVisible(true);
    console.setVisible(false);
  }

  /** Initialises the window.
   */
  private void initWindow() {
    setTitle("KeggBrowser");
    setSize(1280, 720);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setLocationRelativeTo(null);
    setLayout(new BorderLayout());
  }

  /** Initialises the debug console.
   */
  private void initConsole() {
    console = new JConsole();
    getContentPane().add(console, BorderLayout.SOUTH);
  }

  /** Initialises the side lists.
   */
  private void initLists() {
    lists = new JPanel();
    lists.setLayout(new BoxLayout(lists, BoxLayout.Y_AXIS));

    lists.add(new JLabel("Pathways"));
    initMapList();

    lists.add(new JLabel("Organisms"));
    initOrganismList();
    getContentPane().add(lists, BorderLayout.EAST);
    lists.setVisible(false);
  }


  /** Initialises the organism list.
   */
  private void initOrganismList() {
    organismList = new JList<Organism>();

    organismList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    organismList.getSelectionModel().addListSelectionListener(new OrganismListSelectionHandler(this));

    organismList.setFixedCellWidth(300);
    organismList.setVisibleRowCount(-1);

    JScrollPane organismListScroller = new JScrollPane(organismList);
    organismListScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    lists.add(organismListScroller);
  }

  /** Initialises the pathway list.
   */
  private void initMapList() {
    mapList = new JList<String>();

    mapList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    mapList.getSelectionModel().addListSelectionListener(new MapListSelectionHandler(this));

    mapList.setFixedCellWidth(300);
    mapList.setVisibleRowCount(-1);

    JScrollPane mapListScroller = new JScrollPane(mapList);
    mapListScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    lists.add(mapListScroller);
  }

  /** Initialises the menu bar.
   */
  private void initMenuBar() {

    menuBar = new JMenuBar();
    setJMenuBar(menuBar);

    // Ajout du menu File
    JMenu file_menu = new JMenu("File");
    menuBar.add(file_menu);

    final JMenuItem open_item = new JMenuItem("Open File",
      new ImageIcon(KeggBrowser.class.getResource("icons/document-open.png")));
    open_item.addActionListener(new PathwayFileChooserListener(this));
    open_item.setAccelerator(KeyStroke.getKeyStroke('O',
      Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
    file_menu.add(open_item);

    // Ajout du bouton browse
    final JMenuItem browse_item = new JMenuItem("Open Folder",
      new ImageIcon(KeggBrowser.class.getResource("icons/folder-open.png")));
    browse_item.addActionListener(new DirectoryChooserListener(this));
    browse_item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,
      Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()+ActionEvent.ALT_MASK));
    file_menu.add(browse_item);

    // Ajout du bouton save
    final JMenuItem save_item = new JMenuItem("Export graph...",
      new ImageIcon(KeggBrowser.class.getResource("icons/document-save-as.png")));
    save_item.addActionListener(new ScreenshotFileChooserListener(this));
    save_item.setAccelerator(KeyStroke.getKeyStroke('S',
      Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
    file_menu.add(save_item);

    // Ajout du menu View
    JMenu view_menu = new JMenu("View");
    menuBar.add(view_menu);

    // Ajout de la case Show toolbar
    final JCheckBoxMenuItem show_toolbar_item =
      new JCheckBoxMenuItem("Show toolbar", true);
    show_toolbar_item.addItemListener(new ItemListener() {
       public void itemStateChanged(ItemEvent e) {
          toolbar.setVisible(show_toolbar_item.getState());
       }
    });
    view_menu.add(show_toolbar_item);

    // Ajout de la case Show toolbar
    final JCheckBoxMenuItem show_debug_item =
      new JCheckBoxMenuItem("Show debug console", false);
    show_debug_item.addItemListener(new ItemListener() {
       public void itemStateChanged(ItemEvent e) {
          console.setVisible(show_debug_item.getState());
       }
    });
    view_menu.add(show_debug_item);
  }

  /** Initialises the pathway displayer.
   */
  private void initDisplayer() {
    displayer = new PathwayDisplayer();
    getContentPane().add(displayer, BorderLayout.CENTER);
  }

  /** Initialises the toolbar.
   */
  private void initToolBar() {
    toolbar = new KeggBrowserToolBar(this);
    getContentPane().add(toolbar, BorderLayout.WEST);
  }

  /** Return the pathway displayer.
   *
   *  @return the pathway displayer.
   */
  public PathwayDisplayer getDisplayer() {
    return displayer;
  }

  /** Return the current directory.
   *
   *  @return the current directory.
   */
  public File getDirectory() {
    return directory;
  }

  /** Set the current directory.
   *
   *  @param directory the directory to browse.
   */
  public void setDirectory(File directory) {
    this.directory = directory;
    displayer.clear();
    makeMaps();
    lists.setVisible(true);
  }

  /** Get the pathway of the given index.
   *
   *  @param index the index of the map.
   *  @return the map corresponding to the
   *          given index.
   */
  public GenericPathway getMap(Integer index) {
    return maps.get(index);
  }

  /** Returns the list of pathways.
   *
   *  @return the list of pathways.
   */
  public List<GenericPathway> getMaps() {
    return maps;
  }

  /** Returns the JList of pathways.
   *
   *  @return the JList of pathways.
   */
  public JList<String> getMapList() {
    return mapList;
  }

  /** Return the organism at a given index.
   *
   *  @param index the index of the organism.
   *  @return the organism at the given index.
   */
  public Organism getOrganism(Integer index) {
    // return organisms.get(index);
    return OrganismUpdater.getOrganisms(displayer.getPathway()).get(index);
  }

  /** Returns the list of organisms.
   *
   *  @return the list of organisms.
   */
  public List<Organism> getOrganisms() {
    // return organisms;
    return OrganismUpdater.getOrganisms(displayer.getPathway());
  }

  /** Return the JList of organisms.
   *
   *  @return the JList of organisms.
   */
  public JList<Organism> getOrganismList() {
    return organismList;
  }

  /** Returns the toolbar.
   *
   *  @return the toolbar.
   */
  public KeggBrowserToolBar getToolbar() {
    return toolbar;
  }

  /** Fill the pathway list.
   *
   *  Iterates recursively through all KGML files of
   *  the current directory to find pathways. Pathway id
   *  are guessed from file names, so following the KEGG
   *  naming convention and not renaming files is
   *  mandatory.
   */
  private void makeMaps() {

    Set<Integer> mapSet = new TreeSet<Integer>();
    Vector<String> mapListData = new Vector<String>();
    maps = new LinkedList<GenericPathway>();

    Pattern p = Pattern.compile("path_[a-z]+([0-9]+).kgml");

    try {
      for (String file: new Paths(directory.getCanonicalPath(), "**.kgml")) {
        Matcher m = p.matcher(file);
        if (m.find())
          mapSet.add(Integer.parseInt(m.group(1)));
      }
    } catch(IOException e) {}

    GenericPathway generic;
    for (Integer mapId: mapSet) {
      generic = new GenericPathway(mapId);
      maps.add(generic);
      mapListData.add(generic.getTitle());
    }

    mapList.setListData(mapListData);
  }

  /** Fill the organism list.
   *
   *  Wait for the generic pathway to be
   *  loaded completely and the updates the
   *  organism JList with found values.
   */
  private void makeOrganisms() {
    organismList.setListData(new Vector<Organism>());
    //organisms = OrganismUpdater.getOrganisms(displayer.getPathway());
    Timer organisms_updater = new OrganismUpdater(40, this);
    organisms_updater.start();
  }

  public void setDisplayer(PathwayDisplayer displayer) {
    this.displayer = displayer;
    getContentPane().add(displayer, BorderLayout.CENTER);
  }

  public void loadPathway(GenericPathway generic) {

    if (generic.getOrganisms().size() == 0) {
      try {
        Paths paths = new Paths(
          directory.getCanonicalPath(),
          String.format("**/*%1$05d.kgml", generic.getNumber()));
        PathwayUpdater.updatePathwayFromFiles(
          generic, paths.fileIterator(), console);
      } catch (IOException e) {}
    }

    ColorUpdater color_updater = new ColorUpdater(
    5, this, toolbar.getPrimaryColor(), toolbar.getSecondaryColor());
    color_updater.start();

    displayer.setPathway(generic);
    makeOrganisms();

  }

  public void showSinglePathway(SinglePathway pathway) {
    lists.setVisible(false);
    displayer.setPathway(pathway);
    pathway.getGraph().colorDefault(toolbar.getPrimaryColor(), toolbar.getSecondaryColor());
  }

  public static void main(String[] args){

    try {
      UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
    } catch (Exception e) {}

    System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
    new KeggBrowser();
  }

}
