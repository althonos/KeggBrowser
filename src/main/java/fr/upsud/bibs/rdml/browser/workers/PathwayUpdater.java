package fr.upsud.bibs.rdml.browser.workers;

import fr.upsud.bibs.rdml.browser.ui.JConsole;
import fr.upsud.bibs.rdml.browser.ui.PathwayDisplayer;
import fr.upsud.bibs.rdml.kegg.GenericPathway;
import fr.upsud.bibs.rdml.kegg.SinglePathway;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingWorker;
import org.jdom2.Document;
import org.jdom2.input.SAXBuilder;

/** Worker in charge of updating a GenericPathway from
 *  KGML files.
 *
 *  It first loads the SinglePathway corresponding
 *  to the KGML file, and then calls the extend()
 *  method on the GenericPathway object.
 */
public class PathwayUpdater extends SwingWorker<String, Object> {

  /** The KGML file to parse.
   */
  private File file;

  /** The generic pathway to update.
   */
  private GenericPathway pathway;

  /** The local SAXBuilder to use.
   */
  private SAXBuilder sxb;

  /** The pathway displayer to repaint.
   */
  private PathwayDisplayer displayer;

  /** The console in which to log.
   */
  private JConsole console;

  /** A list of active PathwayUpdater instances.
   */
  public static List<PathwayUpdater> active = new ArrayList<>();


  /** Spawn workers to update the generic pathway from an
   *  iterator of files.
   *
   *  @param pathway the pathway to update
   *  @param files an iterator over the files to parse
   *  @param console the console in which to log.
   */
  public static void updatePathwayFromFiles(GenericPathway pathway,
                                            Iterator<File> files,
                                            JConsole console) {
    console.info("Extending GenericPathway");
    while (files.hasNext()) {
      SwingWorker updater = new PathwayUpdater(pathway, files.next(), console);
      updater.execute();
    }
  }

  /** Returns the list of active PathwayUpdater instances.
   *
   *  @return the list of active PathwayUpdater instances.
   */
  public static List<PathwayUpdater> getActive() {
    return active;
  }

  /** Create a new PathwayUpdater worker.
   *
   *  @param pathway the pathway to update.
   *  @param file the file to parse.
   *  @param console the console in which to log.
   */
  public PathwayUpdater(GenericPathway pathway, File file, JConsole console) {
    console.info("Creating updater for: "+file.getName());
    this.file    = file;
    this.pathway = pathway;
    this.sxb = new SAXBuilder();
    sxb.setFeature(
      "http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
    this.console = console;
    active.add(this);
  }

  /** Perform the given task in background.
   *
   *  @return the status of the worker.
   */
  @Override
  protected String doInBackground() {
    console.info("Starting updater for: "+file.getName());
    try {
      Document document = sxb.build(file);
      pathway.extend(new SinglePathway(document.getRootElement()));
      return "ok";
    } catch (Exception e) {
      e.printStackTrace();
      return "errored";
    }
  }

  /** Called when the worker is done.
   */
  @Override
  protected void done() {
    try {
      String status = get();
      console.info("Completed updater for: "+file.getName()+" with status "+status);
    } catch (InterruptedException e) {
       // This is thrown if the thread's interrupted.
       console.info("Interrupted updater for: "+file.getName());
    } catch (ExecutionException e) {
       // This is thrown if we throw an exception
       // from doInBackground.
       console.info("Exception in updater for: "+file.getName());
    } finally {
      active.remove(this);
    }
  }

}
