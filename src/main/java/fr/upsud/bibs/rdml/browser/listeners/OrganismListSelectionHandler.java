package fr.upsud.bibs.rdml.browser.listeners;

import fr.upsud.bibs.rdml.browser.KeggBrowser;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/** The listener that handles organism selection.
 */
public class OrganismListSelectionHandler implements ListSelectionListener {

  /** The KeggBrowser where this handler
   *  is used.
   */
  private KeggBrowser browser;

  /** Create a new OrganismListSelectionHandler.
   *
   *  @param browser a KeggBrowser instance.
   */
  public OrganismListSelectionHandler(KeggBrowser browser) {
    this.browser = browser;
  }

  /** Defines what happens when the selection changes.
   *
   * @param e the selection event.
   */
  @Override
  public void valueChanged(ListSelectionEvent e) {

    ListSelectionModel model = (ListSelectionModel) e.getSource();

    int index = browser.getOrganismList().getSelectedIndex();
    if (!model.isSelectionEmpty() && !e.getValueIsAdjusting())
      browser.getDisplayer().getGraph().colorOrganism(
        browser.getOrganism(index),
        browser.getToolbar().getPrimaryColor(),
        browser.getToolbar().getSecondaryColor());
  }




}
