package fr.upsud.bibs.rdml.browser.workers;

import fr.upsud.bibs.rdml.browser.KeggBrowser;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

/** Timed thread in charge of coloring the
 *  graph with default coloring scheme.
 */
public class ColorUpdater extends Timer implements ActionListener {

  /** A KeggBrowser instance.
   */
  private KeggBrowser browser;

  /** The primary color to use (use for reactions).
   */
  private Color primaryColor;

  /** The secondary color to use (use for compounds).
   */
  private Color secondaryColor;

  /** Create a new ColorUpdater timer.
   *
   *  @param delay how regularly the listener
   *               is called (in milliseconds).
   *  @param browser a KeggBrowser instance.
   *  @param p the primary color to use.
   *  @param s the secondary color to use.
   */
  public ColorUpdater(Integer delay, KeggBrowser browser, Color p, Color s) {
    super(delay, null);
    this.browser = browser;
    this.primaryColor = p;
    this.secondaryColor = s;
    addActionListener(this);
    setRepeats(true);
  }

  /** Called regularly by the timer.
   *
   *  Will color the graph with default coloring at
   *  every call, and stop itself if the PathwayUpdater
   *  threads are all finished.
   *
   *  @param evt an action event.
   */
  @Override
  public void actionPerformed(ActionEvent evt) {
    browser.getDisplayer().getGraph().colorDefault(primaryColor, secondaryColor);
    if (PathwayUpdater.getActive().size() == 0)
      stop();
  }

}
