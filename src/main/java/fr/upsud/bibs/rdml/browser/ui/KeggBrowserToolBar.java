package fr.upsud.bibs.rdml.browser.ui;

import com.bric.colorpicker.ColorPickerDialog;
import fr.upsud.bibs.rdml.browser.KeggBrowser;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Objects;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JToolBar;

/** A toolbar with tools used by KeggBrowser.
 */
public class KeggBrowserToolBar extends JToolBar {

  /** A KeggBrowser instance.
   */
  private KeggBrowser browser;

  /** The Primary color used by the
   *  program.
   */
  private Color primaryColor = Color.RED;

  /** The secondaryColor used by the program.
   */
  private Color secondaryColor = Color.GREEN;

  /** The label displaying the primary color.
   */
  private JLabel primaryColorLabel;

  /** The label displaying the secondary color.
   */
  private JLabel secondaryColorLabel;

  /** Create a new Toolbar.
   *
   *  @param browser a KeggBrowser instance.
   */
  public KeggBrowserToolBar(KeggBrowser browser) {
    super(JToolBar.VERTICAL);
    this.browser = browser;

    // TOGGLE COMPOUND LABEL button
    JButton show_compound_labels = new JButton(new ImageIcon(
      KeggBrowser.class.getResource("icons/compound-text.png")));
    show_compound_labels.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          if (getBrowser().getDisplayer().getGraph() != null)
            getBrowser().getDisplayer().getGraph().toggleCompoundLabels();
        }
    });
    show_compound_labels.setToolTipText("Toggle compound labels.");
    super.add(show_compound_labels);

    // TOGGLE REACTION LABEL button
    JButton show_reaction_labels = new JButton(new ImageIcon(
      KeggBrowser.class.getResource("icons/reaction-text.png")));
    show_reaction_labels.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          if (getBrowser().getDisplayer().getGraph() != null)
            getBrowser().getDisplayer().getGraph().toggleReactionLabels();
        }
    });
    show_reaction_labels.setToolTipText("Toggle reaction labels.");
    super.add(show_reaction_labels);

    // SEPARATOR
    super.add(new JToolBar.Separator());

    // COLOR DEFAULT button
    JButton color_default = new JButton(new ImageIcon(
      KeggBrowser.class.getResource("icons/image-color-default.png")));
    color_default.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          if (getBrowser().getDisplayer().getGraph() != null)
            getBrowser().getDisplayer().getGraph().colorDefault(
              primaryColor, secondaryColor);
        }
    });
    color_default.setToolTipText("Color graph with default coloring.");
    super.add(color_default);

    // COLOR CONSERVARTION button
    JButton color_conservation = new JButton(new ImageIcon(
      KeggBrowser.class.getResource("icons/image-color-conservation.png")));
    color_conservation.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          if (getBrowser().getDisplayer().getGraph() != null)
            getBrowser().getDisplayer().getGraph().colorConservation(
              secondaryColor, primaryColor);
        }
    });
    color_conservation.setToolTipText("Color graph based on conservation"
      +" of reactions and compounds among organisms.");
    super.add(color_conservation);

    // SEPARATOR
    super.add(new JToolBar.Separator());
    super.add(Box.createHorizontalGlue());
    super.add(Box.createVerticalGlue());

    primaryColorLabel = new JLabel();
    primaryColorLabel.setBackground(primaryColor);
    primaryColorLabel.setPreferredSize(new Dimension(32, 24));
    primaryColorLabel.setOpaque(true);
    primaryColorLabel.addMouseListener(new MouseAdapter() {
        public void mouseClicked(MouseEvent e) {
          Color newColor = ColorPickerDialog.showDialog(
               KeggBrowserToolBar.this.browser,
               "Select Primary Color", primaryColor, false);
          if(newColor != null)
              setPrimaryColor(newColor);
      }
    });
    super.add(primaryColorLabel);

    JLabel switchColorLabel = new JLabel(new ImageIcon(
      KeggBrowser.class.getResource("icons/exchange-positions.png")));
    switchColorLabel.setPreferredSize(new Dimension(32, 32));
    switchColorLabel.addMouseListener(new MouseAdapter() {
        public void mouseClicked(MouseEvent e) {switchColors();}
    });
    super.add(switchColorLabel);


    secondaryColorLabel = new JLabel();
    secondaryColorLabel.setBackground(secondaryColor);
    secondaryColorLabel.setPreferredSize(new Dimension(32, 24));
    secondaryColorLabel.setOpaque(true);
    secondaryColorLabel.addMouseListener(new MouseAdapter() {
        public void mouseClicked(MouseEvent e) {
          Color newColor = ColorPickerDialog.showDialog(
               KeggBrowserToolBar.this.browser,
               "Select Primary Color", secondaryColor, false);
          if(newColor != null)
              setSecondaryColor(newColor);
      }
    });
    super.add(secondaryColorLabel);

    super.add(Box.createHorizontalStrut(32));
    super.add(Box.createVerticalStrut(24*3));

    super.setVisible(true);
    super.setFloatable(false);
  }

  /** Returns the primary color value.
   *
   * @return the primary color value.
   */
  public Color getPrimaryColor() {
    return primaryColor;
  }

  /** Set a new primary color value.
   *
   *  @param color the new primary color
   *               to use.
   */
  public void setPrimaryColor(Color color) {
    Objects.requireNonNull(color);
    primaryColor = color;
    primaryColorLabel.setBackground(color);
    repaint();
  }

  /** Returns the secondary color value.
   *
   *  @return the secondary color value.
   */
  public Color getSecondaryColor() {
    return secondaryColor;
  }

  /** Set a new secondary color value.
   *
   *  @param color the new secondary color value.
   */
  public void setSecondaryColor(Color color) {
    Objects.requireNonNull(color);
    secondaryColor = color;
    secondaryColorLabel.setBackground(color);
    repaint();
  }

  /** Exchange the values of the primary
   *  and secondary colors.
   */
  public void switchColors() {
    Color temp = secondaryColor;
    setSecondaryColor(primaryColor);
    setPrimaryColor(temp);
  }

  /** Get the associated KeggBrowser isntance.
   *
   *  @return the browser this toolbar is
   *          displayed in.
   */
  public KeggBrowser getBrowser() {
    return browser;
  }


}
