package fr.upsud.bibs.rdml.browser.ui;

import fr.upsud.bibs.rdml.graph.PathwayGraph;
import fr.upsud.bibs.rdml.kegg.Pathway;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import javax.swing.JPanel;
import org.graphstream.graph.Node;
import org.graphstream.ui.geom.Point3;
import org.graphstream.ui.graphicGraph.GraphPosLengthUtils;
import org.graphstream.ui.view.Camera;
import org.graphstream.ui.view.View;
import org.graphstream.ui.view.Viewer;

/** A JPanel made to render a Pathway object.
 */
public class PathwayDisplayer extends JPanel implements MouseListener {

  /** A Viewer instance.
   */
  private Viewer viewer;

  /** The main component of the panel.
   */
  private View view;

  /** The pathway graph to display.
   */
  protected PathwayGraph graph;

  /** The pathway to display.
   */
  protected Pathway pathway;

  /** Create a new PathwayDisplayer.
   */
  public PathwayDisplayer() {
    super();
    setLayout(new BorderLayout());
    setOpaque(true);
    setBackground(Color.WHITE);
  }

  /** Create a new PathwayDisplayer with
   *  a preset pathway.
   *
   *  @param pathway the pathway to display.
   */
  public PathwayDisplayer(Pathway pathway) {
    this();
    setGraph(pathway.getGraph());
  }

  /** Returns the viewer of the displayer.
   *
   *  @return the viewer of the displayer.
   */
  public Viewer getViewer() {
    return viewer;
  }

  /** Returns the viewer of the displayer.
   *
   *  @return the viewer of the displayer.
   */
  public View getView() {
    return view;
  }

  /** Returns the graph of the displayer.
   *
   *  @return the graph of the displayer.
   */
  public PathwayGraph getGraph() {
    return graph;
  }

  /** Return the pathway of the displayer.
   *
   *  @return the pathway of the displayer.
   */
  public Pathway getPathway() {
    return pathway;
  }

  /** Set a new Pathway.
   *
   *  @param pathway the new pathway to display.
   */
  public void setPathway(Pathway pathway) {
    clear();
    this.pathway = pathway;
    setGraph(pathway.getGraph());
  }

  /** Remove the pathway from the displayer
   *  if any, and clear the JPanel.
   */
  public void clear() {
    if (view != null) {
      remove((Component) view);
      repaint();
      revalidate();
    }

    graph = null;
    viewer = null;
    view = null;
    pathway = null;
  }

  /** Set the graph to display.
   *
   *  @param graph the graph to display.
   */
  public void setGraph(PathwayGraph graph) {
    // Set the new graph and create a new View / Viewer
    this.graph = graph;
    viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);

    // Create the view and make it monitor clicks
    view = viewer.addDefaultView(false);
    view.addMouseListener(this);

    // Add the view to self and make it visible
    add((Component) view);
    setVisible(true);

    // Refresh the window
    repaint();
    revalidate();
  }

  /** Defines what happens when the mouse
   *  leaves the display area.
   *
   *  @param e a mouse event.
   */
  @Override
  public void mouseExited(MouseEvent e) {}

  /** Defines what happens when the mouse
   *  enters the display area.
   *
   *  @param e a mouse event.
   */
  @Override
  public void mouseEntered(MouseEvent e) {}

  /** Defines what happens when the mouse
   *  is pressed.
   *
   *  @param e a mouse event.
   */
  @Override
  public void mousePressed(MouseEvent e) {}

  /** Defines what happens when the mouse
   *  is pressed.
   *
   *  @param e a mouse event.
   */
  @Override
  public void mouseReleased(MouseEvent e) {}

  /** Defines what happens when the displayer
   *  is clicked.
   *
   *  @param e a mouse event.
   */
  @Override
  public void mouseClicked(MouseEvent e) {

    if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2) {
      Camera c = view.getCamera();

      // Check if a node was clicked.
      for (Node node : viewer.getGraphicGraph().getEachNode()) {

        double[] xyz = GraphPosLengthUtils.nodePosition(node);
        Point3 point = c.transformGuToPx(xyz[0], xyz[1], xyz[2]);
        double dx = point.x-e.getX();
        double dy = point.y-e.getY();

        // Check the distance from the cursor to the node
        if ( dx*dx + dy*dy < 30.0 ) {
          String name = node.getAttribute("ui.label");

          // Get the URL of the clicked node
          URL url = "reaction".equals(node.getAttribute("ui.class")) ?
                    pathway.getReaction(name).getURL() :
                    pathway.getCompound(name).getURL();

          // Open the URL
          openWebpage(url);
          return;
        }
      }
    }
  }


  /** Open the web page in the OS browser
   *  referenced by the given URL.
   *
   *  @param url a website URL.
   */
  public static void openWebpage(URL url) {
      System.out.println(url);
      Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
      if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
          try {
              desktop.browse(url.toURI());
          } catch (Exception e) {
              e.printStackTrace();
          }
      }
  }

}
