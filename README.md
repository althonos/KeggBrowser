# KeggBrowser

*A KEGG metabolic pathway browser for the desktop*

[![Travis branch](https://img.shields.io/travis/althonos/KeggBrowser/master.svg?style=flat-square)](https://travis-ci.org/althonos/KeggBrowser)
[![Dependency Status](https://www.versioneye.com/user/projects/59318fb280def1005264f671/badge.svg?style=flat-square)](https://www.versioneye.com/user/projects/59318fb280def1005264f671)
[![Codecov branch](https://img.shields.io/codecov/c/github/althonos/KeggBrowser/master.svg?style=flat-square)](https://codecov.io/gh/althonos/KeggBrowser)
[![Codacy branch grade](https://img.shields.io/codacy/grade/733111c8acf64f338bd6831521e4f4cf/master.svg?style=flat-square)](https://www.codacy.com/app/althonos/KeggBrowser/dashboard)
[![License](https://img.shields.io/badge/license-GPLv3-blue.svg?style=flat-square)](https://choosealicense.com/licenses/gpl-3.0/)
